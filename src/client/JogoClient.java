package client;

import java.util.Scanner;

import generated.Jogo;
import generated.JogoServerService;


public class JogoClient {
	
	private static String nome;
	private static Scanner scanner = new Scanner(System.in);

	public static String lerNome(){
		System.out.print("Digite seu nome: ");
		nome = scanner.nextLine();
		while (nome == null || nome.trim().equals("")){
			System.out.println("Nome n�o pode ser vazio");
			System.out.print("Digite novamente: ");
			nome = scanner.nextLine();
		}
		return nome;
	}
	
	public static void exibirResposta(String resposta){
		if(resposta != null && resposta.trim() != "")
			System.out.println(resposta);
	}
	
	public static Integer lerEscolha(){
		String resposta = null;
		System.out.print("Digite a opcao escolhida:");
		resposta = scanner.next();
		while(!validarEscolha(resposta)){
			System.out.println("Opcao invalida, digite novamente");
			resposta = scanner.nextLine();
		}
		return Integer.valueOf(resposta);
	}
	
	public static Boolean validarEscolha(String escolha){
		if (escolha.equals("1") ||escolha.equals("2")
				|| escolha.equals("3")|| escolha.equals("5")
				|| escolha.equals("4")){
			return true;
		} else
			return false;
	}
	
	public static void main(String[] args) {
		JogoServerService factory = new JogoServerService();
		Jogo jogo = factory.getJogoPort();
		
		if(!jogo.conectar().equals("servidor cheio")){
			exibirResposta(jogo.conectar());
			nome = lerNome();
			exibirResposta(jogo.informarNomeJogador(nome));
			while(!jogo.fimDaPartida()){
				if(jogo.podeJogar(nome) && !jogo.fimDaRodada()){
					exibirResposta(jogo.getOpcoes());
					Integer escolha = lerEscolha();
					exibirResposta(jogo.informarEscolha(escolha, nome));
				}else if(jogo.fimDaRodada() && !jogo.podeJogar(nome)){					
					exibirResposta(jogo.getResultadoRodada(nome));
				}
			}
			exibirResposta(jogo.getResultadoFinal());
		}else{
			System.out.println("Servidor cheio, tente novamente mais tarde.");
			System.exit(0);
		}		
	}
}
