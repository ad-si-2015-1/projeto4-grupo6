
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FimDaRodadaResponse_QNAME = new QName("http://server/", "fimDaRodadaResponse");
    private final static QName _GetOpcoes_QNAME = new QName("http://server/", "getOpcoes");
    private final static QName _ConectarResponse_QNAME = new QName("http://server/", "conectarResponse");
    private final static QName _Conectar_QNAME = new QName("http://server/", "conectar");
    private final static QName _FinalizarJogoResponse_QNAME = new QName("http://server/", "finalizarJogoResponse");
    private final static QName _FimDaRodada_QNAME = new QName("http://server/", "fimDaRodada");
    private final static QName _FimDaPartidaResponse_QNAME = new QName("http://server/", "fimDaPartidaResponse");
    private final static QName _InformarNomeJogadorResponse_QNAME = new QName("http://server/", "informarNomeJogadorResponse");
    private final static QName _GetResultadoRodada_QNAME = new QName("http://server/", "getResultadoRodada");
    private final static QName _InformarNomeJogador_QNAME = new QName("http://server/", "informarNomeJogador");
    private final static QName _PodeJogarResponse_QNAME = new QName("http://server/", "podeJogarResponse");
    private final static QName _InformarEscolhaResponse_QNAME = new QName("http://server/", "informarEscolhaResponse");
    private final static QName _InformarEscolha_QNAME = new QName("http://server/", "informarEscolha");
    private final static QName _PodeJogar_QNAME = new QName("http://server/", "podeJogar");
    private final static QName _GetResultadoRodadaResponse_QNAME = new QName("http://server/", "getResultadoRodadaResponse");
    private final static QName _GetOpcoesResponse_QNAME = new QName("http://server/", "getOpcoesResponse");
    private final static QName _GetResultadoFinal_QNAME = new QName("http://server/", "getResultadoFinal");
    private final static QName _FinalizarJogo_QNAME = new QName("http://server/", "finalizarJogo");
    private final static QName _FimDaPartida_QNAME = new QName("http://server/", "fimDaPartida");
    private final static QName _GetResultadoFinalResponse_QNAME = new QName("http://server/", "getResultadoFinalResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetResultadoFinal }
     * 
     */
    public GetResultadoFinal createGetResultadoFinal() {
        return new GetResultadoFinal();
    }

    /**
     * Create an instance of {@link FinalizarJogo }
     * 
     */
    public FinalizarJogo createFinalizarJogo() {
        return new FinalizarJogo();
    }

    /**
     * Create an instance of {@link InformarEscolha }
     * 
     */
    public InformarEscolha createInformarEscolha() {
        return new InformarEscolha();
    }

    /**
     * Create an instance of {@link PodeJogar }
     * 
     */
    public PodeJogar createPodeJogar() {
        return new PodeJogar();
    }

    /**
     * Create an instance of {@link GetResultadoRodadaResponse }
     * 
     */
    public GetResultadoRodadaResponse createGetResultadoRodadaResponse() {
        return new GetResultadoRodadaResponse();
    }

    /**
     * Create an instance of {@link GetOpcoesResponse }
     * 
     */
    public GetOpcoesResponse createGetOpcoesResponse() {
        return new GetOpcoesResponse();
    }

    /**
     * Create an instance of {@link FimDaPartida }
     * 
     */
    public FimDaPartida createFimDaPartida() {
        return new FimDaPartida();
    }

    /**
     * Create an instance of {@link GetResultadoFinalResponse }
     * 
     */
    public GetResultadoFinalResponse createGetResultadoFinalResponse() {
        return new GetResultadoFinalResponse();
    }

    /**
     * Create an instance of {@link PodeJogarResponse }
     * 
     */
    public PodeJogarResponse createPodeJogarResponse() {
        return new PodeJogarResponse();
    }

    /**
     * Create an instance of {@link InformarNomeJogador }
     * 
     */
    public InformarNomeJogador createInformarNomeJogador() {
        return new InformarNomeJogador();
    }

    /**
     * Create an instance of {@link InformarEscolhaResponse }
     * 
     */
    public InformarEscolhaResponse createInformarEscolhaResponse() {
        return new InformarEscolhaResponse();
    }

    /**
     * Create an instance of {@link GetResultadoRodada }
     * 
     */
    public GetResultadoRodada createGetResultadoRodada() {
        return new GetResultadoRodada();
    }

    /**
     * Create an instance of {@link InformarNomeJogadorResponse }
     * 
     */
    public InformarNomeJogadorResponse createInformarNomeJogadorResponse() {
        return new InformarNomeJogadorResponse();
    }

    /**
     * Create an instance of {@link Conectar }
     * 
     */
    public Conectar createConectar() {
        return new Conectar();
    }

    /**
     * Create an instance of {@link ConectarResponse }
     * 
     */
    public ConectarResponse createConectarResponse() {
        return new ConectarResponse();
    }

    /**
     * Create an instance of {@link FimDaRodada }
     * 
     */
    public FimDaRodada createFimDaRodada() {
        return new FimDaRodada();
    }

    /**
     * Create an instance of {@link FinalizarJogoResponse }
     * 
     */
    public FinalizarJogoResponse createFinalizarJogoResponse() {
        return new FinalizarJogoResponse();
    }

    /**
     * Create an instance of {@link FimDaRodadaResponse }
     * 
     */
    public FimDaRodadaResponse createFimDaRodadaResponse() {
        return new FimDaRodadaResponse();
    }

    /**
     * Create an instance of {@link GetOpcoes }
     * 
     */
    public GetOpcoes createGetOpcoes() {
        return new GetOpcoes();
    }

    /**
     * Create an instance of {@link FimDaPartidaResponse }
     * 
     */
    public FimDaPartidaResponse createFimDaPartidaResponse() {
        return new FimDaPartidaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FimDaRodadaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "fimDaRodadaResponse")
    public JAXBElement<FimDaRodadaResponse> createFimDaRodadaResponse(FimDaRodadaResponse value) {
        return new JAXBElement<FimDaRodadaResponse>(_FimDaRodadaResponse_QNAME, FimDaRodadaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOpcoes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "getOpcoes")
    public JAXBElement<GetOpcoes> createGetOpcoes(GetOpcoes value) {
        return new JAXBElement<GetOpcoes>(_GetOpcoes_QNAME, GetOpcoes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConectarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "conectarResponse")
    public JAXBElement<ConectarResponse> createConectarResponse(ConectarResponse value) {
        return new JAXBElement<ConectarResponse>(_ConectarResponse_QNAME, ConectarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Conectar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "conectar")
    public JAXBElement<Conectar> createConectar(Conectar value) {
        return new JAXBElement<Conectar>(_Conectar_QNAME, Conectar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinalizarJogoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "finalizarJogoResponse")
    public JAXBElement<FinalizarJogoResponse> createFinalizarJogoResponse(FinalizarJogoResponse value) {
        return new JAXBElement<FinalizarJogoResponse>(_FinalizarJogoResponse_QNAME, FinalizarJogoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FimDaRodada }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "fimDaRodada")
    public JAXBElement<FimDaRodada> createFimDaRodada(FimDaRodada value) {
        return new JAXBElement<FimDaRodada>(_FimDaRodada_QNAME, FimDaRodada.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FimDaPartidaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "fimDaPartidaResponse")
    public JAXBElement<FimDaPartidaResponse> createFimDaPartidaResponse(FimDaPartidaResponse value) {
        return new JAXBElement<FimDaPartidaResponse>(_FimDaPartidaResponse_QNAME, FimDaPartidaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarNomeJogadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "informarNomeJogadorResponse")
    public JAXBElement<InformarNomeJogadorResponse> createInformarNomeJogadorResponse(InformarNomeJogadorResponse value) {
        return new JAXBElement<InformarNomeJogadorResponse>(_InformarNomeJogadorResponse_QNAME, InformarNomeJogadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResultadoRodada }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "getResultadoRodada")
    public JAXBElement<GetResultadoRodada> createGetResultadoRodada(GetResultadoRodada value) {
        return new JAXBElement<GetResultadoRodada>(_GetResultadoRodada_QNAME, GetResultadoRodada.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarNomeJogador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "informarNomeJogador")
    public JAXBElement<InformarNomeJogador> createInformarNomeJogador(InformarNomeJogador value) {
        return new JAXBElement<InformarNomeJogador>(_InformarNomeJogador_QNAME, InformarNomeJogador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PodeJogarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "podeJogarResponse")
    public JAXBElement<PodeJogarResponse> createPodeJogarResponse(PodeJogarResponse value) {
        return new JAXBElement<PodeJogarResponse>(_PodeJogarResponse_QNAME, PodeJogarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarEscolhaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "informarEscolhaResponse")
    public JAXBElement<InformarEscolhaResponse> createInformarEscolhaResponse(InformarEscolhaResponse value) {
        return new JAXBElement<InformarEscolhaResponse>(_InformarEscolhaResponse_QNAME, InformarEscolhaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InformarEscolha }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "informarEscolha")
    public JAXBElement<InformarEscolha> createInformarEscolha(InformarEscolha value) {
        return new JAXBElement<InformarEscolha>(_InformarEscolha_QNAME, InformarEscolha.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PodeJogar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "podeJogar")
    public JAXBElement<PodeJogar> createPodeJogar(PodeJogar value) {
        return new JAXBElement<PodeJogar>(_PodeJogar_QNAME, PodeJogar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResultadoRodadaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "getResultadoRodadaResponse")
    public JAXBElement<GetResultadoRodadaResponse> createGetResultadoRodadaResponse(GetResultadoRodadaResponse value) {
        return new JAXBElement<GetResultadoRodadaResponse>(_GetResultadoRodadaResponse_QNAME, GetResultadoRodadaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOpcoesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "getOpcoesResponse")
    public JAXBElement<GetOpcoesResponse> createGetOpcoesResponse(GetOpcoesResponse value) {
        return new JAXBElement<GetOpcoesResponse>(_GetOpcoesResponse_QNAME, GetOpcoesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResultadoFinal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "getResultadoFinal")
    public JAXBElement<GetResultadoFinal> createGetResultadoFinal(GetResultadoFinal value) {
        return new JAXBElement<GetResultadoFinal>(_GetResultadoFinal_QNAME, GetResultadoFinal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinalizarJogo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "finalizarJogo")
    public JAXBElement<FinalizarJogo> createFinalizarJogo(FinalizarJogo value) {
        return new JAXBElement<FinalizarJogo>(_FinalizarJogo_QNAME, FinalizarJogo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FimDaPartida }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "fimDaPartida")
    public JAXBElement<FimDaPartida> createFimDaPartida(FimDaPartida value) {
        return new JAXBElement<FimDaPartida>(_FimDaPartida_QNAME, FimDaPartida.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResultadoFinalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server/", name = "getResultadoFinalResponse")
    public JAXBElement<GetResultadoFinalResponse> createGetResultadoFinalResponse(GetResultadoFinalResponse value) {
        return new JAXBElement<GetResultadoFinalResponse>(_GetResultadoFinalResponse_QNAME, GetResultadoFinalResponse.class, null, value);
    }

}
