package server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(name = "Jogo")
public class JogoServer {
	
	private Jogador jogadorUm;
	private Jogador jogadorDois;
	private Boolean fimRodada;
	private Boolean fimPartida;
	private Integer numeroRodada;
	private String resultadoRodada;
	
	public JogoServer(){
		this.init();
	}
	
	@WebMethod(exclude = true)
	public void init(){
		jogadorUm = new Jogador();
		jogadorDois = new Jogador();
		fimRodada = false;
		fimPartida = false;
		resultadoRodada = null;
		numeroRodada = 1;
	}
	
	public String conectar(){
		if(jogadorUm.ativo() && jogadorDois.ativo()){
			return "servidor cheio";
		}else{
			return "Bem vindo ao jogo Pedra, Papel, Tesoura, Largarto e Spock!";
		}
	}
	
	public String informarNomeJogador(String nome){
		if(!jogadorUm.ativo()){
			jogadorUm.setAtivo(true);
			jogadorUm.setNome(nome);
			return "Bem vindo ao jogo " + nome + " aguarde um oponente!";
		}else{
			jogadorDois.setAtivo(true);
			jogadorDois.setNome(nome);
			return "Bem vindo ao jogo " + nome + " o jogo iniciara em instantes!";
		}
	}
	
	public String getOpcoes(){
		String opcoes = "\r\nRODADA " + numeroRodada +"!!\r\n\r\n"
				+ "1-Pedra "
				+ "2-Papel "
				+ "3-Tesoura "
				+ "4-Lagarto "
				+ "5-Spock\r\n";
		return opcoes;
	}
	
	public Boolean podeJogar(String nome){
		if(jogadorUm.getNome().equals(nome))
			return jogadorUm.getPodeJogar();
		else
			return jogadorDois.getPodeJogar();
	}
	
	public String informarEscolha(Integer escolha, String nome){
		Jogador jogador;
		if(nome.equals(jogadorUm.getNome()))
			jogador = jogadorUm;
		else
			jogador = jogadorDois;
		switch(escolha){
		case 1:
			jogador.setEscolha("pedra");
			break;
		case 2:
			jogador.setEscolha("papel");
			break;
		case 3:
			jogador.setEscolha("tesoura");
			break;
		case 4:
			jogador.setEscolha("lagarto");
			break;
		case 5:
			jogador.setEscolha("spock");
			break;
		}
		jogador.setPodeJogar(false);
		if(jogadorUm.getEscolha() != null && jogadorDois.getEscolha() != null){
			avaliarVencedorRodada();
			fimRodada = true;
		}
		return "Aguarde seu oponente jogar!";
	}
	
	public Boolean fimDaRodada(){
		return fimRodada;
	}
	
	public void avaliarVencedorRodada(){
		String nomeVencedor = null;
		
		if(jogadorUm.getEscolha() == jogadorDois.getEscolha()){
			this.resultadoRodada = "Ambos escolheram a mesma opcaoo, empate!!";
		}else{
			switch(jogadorUm.getEscolha().trim()){
			case "tesoura":
				if(jogadorDois.getEscolha() == "papel" || jogadorDois.getEscolha() == "lagarto"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor = jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor = jogadorDois.getNome();
				}
				break;
			case "spock":
				if(jogadorDois.getEscolha() == "tesoura" || jogadorDois.getEscolha() == "pedra"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor=jogadorDois.getNome();
				}
				break;
			case "pedra":
				if(jogadorDois.getEscolha() == "tesoura" || jogadorDois.getEscolha() == "lagarto"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor=jogadorDois.getNome();
				}
				break;
			case "lagarto":
				if(jogadorDois.getEscolha() == "papel" || jogadorDois.getEscolha() == "spock"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor=jogadorDois.getNome();
				}
				break;
			case "papel":
				if(jogadorDois.getEscolha() == "pedra" || jogadorDois.getEscolha() == "spock"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					nomeVencedor=jogadorUm.getNome();
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					nomeVencedor = jogadorDois.getNome();
				}
				break;
			}
			
			if(jogadorUm.getPontuacao() > 2 || jogadorDois.getPontuacao() > 2){
				fimPartida = true;
			}
			
			
			resultadoRodada = String.format("\n%s venceu essa rodada!!\n\rPlacar: %s %d X %d %s\r\nIniciando rodada %d\n",
					nomeVencedor, jogadorUm.getNome().toUpperCase(), jogadorUm.getPontuacao(),
					jogadorDois.getPontuacao(), jogadorDois.getNome().toUpperCase(), numeroRodada+1);
		}
		this.jogadorUm.setEscolha(null);
		this.jogadorDois.setEscolha(null);
	}
	
	public String getResultadoRodada(String nome) {
		Jogador jogador;
		if (nome.equals(jogadorUm.getNome()))
			jogador = jogadorUm;
		else
			jogador = jogadorDois;


		jogador.setPodeJogar(true);
		
		if(jogadorUm.getPodeJogar() && jogadorDois.getPodeJogar()){
			fimRodada = false;
			numeroRodada++;
		}
		
		return resultadoRodada;
	}
	
	public Boolean fimDaPartida(){
		return fimPartida;
	}
	
	public String getResultadoFinal(){
		String vencedor = "";
		
		if (jogadorUm.getPontuacao() > jogadorDois.getPontuacao())
			vencedor = jogadorUm.getNome();
		else
			vencedor = jogadorDois.getNome();
		
		return String.format("\n%s VENCEU A PARTIDA!!\n\rPlacar final: %s %d X %d %s\r\nFim de jogo!\n\r"
			+ "Parabens!! %s\n\n",
			vencedor.toUpperCase(), jogadorUm.getNome().toUpperCase(), jogadorUm.getPontuacao(),
			jogadorDois.getPontuacao(), jogadorDois.getNome().toUpperCase(), vencedor.toUpperCase());
	}
	
	public String finalizarJogo(String nome){
		this.init();
		return "Finalizando partida\n";
	}
	
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:54321/jogo", new JogoServer());
		System.out.println("Servidor iniciado!");
	}
}
