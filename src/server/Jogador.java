package server;

public class Jogador {
	private String escolha;
	private String nome;
	private Integer pontuacao;
	private Boolean podeJogar;
	private Boolean ativo;
	
	public Jogador(){
		this.resetar();
	}
	
	public void resetar(){
		this.pontuacao = 0;
		this.escolha = null;
		this.nome = null;
		this.podeJogar = true;
		this.ativo = false;
	}

	public String getEscolha() {
		return escolha;
	}

	public void setEscolha(String escolha) {
		this.escolha = escolha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}

	public Boolean getPodeJogar() {
		return podeJogar;
	}

	public void setPodeJogar(Boolean podeJogar) {
		this.podeJogar = podeJogar;
	}

	public Boolean ativo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}